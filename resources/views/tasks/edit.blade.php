@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        @include('partials.nav')
                    </div>
                </div>

            </div>
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{route('tasks.update', $task->id)}}" method="post">
                            {{ method_field('PATCH') }}

                            {{ csrf_field() }}

                            <input type="hidden" name="id" value="{{ $task->id }}">
                            <div class="form-group">
                                <label for="project"> Select a project (optional) </label>
                                <select name="project_id" id="project" class="form-control">
                                    <option value="">No Project</option>
                                    @foreach($projects as $project)
                                        <option
                                                value="{{$project->id}}"
                                                @if($project->id == $task->project_id) selected="selected" @endif >
                                            {{ $project->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="task">Task</label>
                                <input type="text"
                                       class="form-control"
                                       id="task"
                                       name="name"
                                       placeholder="Enter Task"
                                       value="{{ $task->name }}">
                            </div>

                            <div class="form-group">
                                <label for="priority">Priority</label>
                                <input type="number"
                                       class="form-control"
                                       id="priority"
                                       value="{{ $task->priority }}"
                                       name="priority">
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection()