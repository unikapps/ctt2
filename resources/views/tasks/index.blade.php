@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3 p-1">
                <div class="card">
                    <div class="card-body">
                        @include('partials.nav')
                        <hr>
                        <h5>Filter by project</h5>
                        <form method="get" id="filter-form">
                            <select name="project" class="form-control">
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}" >{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </form>

                    </div>
                </div>

            </div>
            <div class="col-sm-9 p-1">
                <div class="card">
                    <div class="card-body">
                        @if(!$tasks)
                            There is no task yet, maybe it's time to
                            <a href="{{ route('tasks.create') }}">add new task</a>

                        @else
                            <div class="list-group" id="tasks" disabled>
                                @foreach($tasks as $task)
                                    <div class="list-group-item" data-id="{{ $task->id }}">

                                        <h3>{{ $task->name }}
                                            <small class="text-muted pl-1">priority: {{ $task->priority }} </small>
                                        </h3>

                                        <div class="ml-auto">

                                            <a class="btn btn-sm btn-outline-dark ml-2 float-right"
                                               href="{{ route('tasks.edit', $task->id ) }}"> edit </a>

                                            <form action="{{ route('tasks.destroy', $task->id) }}" method="post"
                                                  onsubmit="return confirm('Are you sure about that ?')"
                                                  class="float-right">

                                                {{ method_field('delete') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-sm btn-outline-danger" type="submit">delete
                                                </button>

                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection()
